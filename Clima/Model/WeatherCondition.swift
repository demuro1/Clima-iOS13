//
//  WeatherCondition.swift
//  Clima
//
//  Created by demuro1 on 2/23/21.
//  Copyright © 2021 Jason De Muro. All rights reserved.
//

import Foundation

struct WeatherCondition{
    let code200 = Condition(code: 200, cat: "Thunderstorm", desc: "thunderstorm with light rain", title: "cloud.bolt.rain")
    let code201 = Condition(code: 201, cat: "Thunderstorm", desc: "thunderstorm with rain", title: "cloud.bolt.rain")
    let code202 = Condition(code: 202, cat: "Thunderstorm", desc: "thunderstorm with heavy rain", title: "cloud.bolt.rain")
    let code210 = Condition(code: 210, cat: "Thunderstorm", desc: "light thunderstorm", title: "cloud.bolt")
    let code211 = Condition(code: 211, cat: "Thunderstorm", desc: "thunderstorm", title: "cloud.bolt")
    let code212 = Condition(code: 212, cat: "Thunderstorm", desc: "heavy thunderstorm", title: "cloud.bolt")
    let code221 = Condition(code: 221, cat: "Thunderstorm", desc: "ragged thunderstorm", title: "cloud.bolt")
    let code230 = Condition(code: 230, cat: "Thunderstorm", desc: "thunderstorm with light drizzle", title: "cloud.bolt.rain")
    let code231 = Condition(code: 231, cat: "Thunderstorm", desc: "thunderstorm with drizzle", title: "cloud.bolt.rain")
    let code232 = Condition(code: 232, cat: "Thunderstorm", desc: "thunderstorm with heavy drizzle", title: "cloud.bolt.rain")
    let code300 = Condition(code: 300, cat: "Drizzle", desc: "light intensity drizzle", title: "cloud.drizzle")
    let code301 = Condition(code: 301, cat: "Drizzle", desc: "drizzle", title: "cloud.drizzle")
    let code302 = Condition(code: 302, cat: "Drizzle", desc: "heavy intensity drizzle", title: "cloud.drizzle")
    let code310 = Condition(code: 310, cat: "Drizzle", desc: "light intensity drizzle rain", title: "cloud.drizzle")
    let code311 = Condition(code: 311, cat: "Drizzle", desc: "drizzle rain", title: "cloud.drizzle")
    let code312 = Condition(code: 312, cat: "Drizzle", desc: "heavy intensity drizzle rain", title: "cloud.drizzle")
    let code313 = Condition(code: 313, cat: "Drizzle", desc: "shower rain and drizzle", title: "cloud.drizzle")
    let code314 = Condition(code: 314, cat: "Drizzle", desc: "heavy shower rain and drizzle", title: "cloud.drizzle")
    let code321 = Condition(code: 321, cat: "Drizzle", desc: "shower drizzle", title: "cloud.drizzle")
    let code500 = Condition(code: 500, cat: "Rain", desc: "light rain", title: "cloud.drizzle")
    let code501 = Condition(code: 501, cat: "Rain", desc: "moderate rain", title: "cloud.rain")
    let code502 = Condition(code: 502, cat: "Rain", desc: "heavy intensity rain", title: "cloud.heavyrain")
    let code503 = Condition(code: 503, cat: "Rain", desc: "very heavy rain", title: "cloud.heavyrain")
    let code504 = Condition(code: 504, cat: "Rain", desc: "extreme rain", title: "cloud.heavyrain")
    let code511 = Condition(code: 511, cat: "Rain", desc: "freezing rain", title: "cloud.sleet")
    let code520 = Condition(code: 520, cat: "Rain", desc: "light intensity shower rain", title: "cloud.rain")
    let code521 = Condition(code: 521, cat: "Rain", desc: "shower rain", title: "cloud.heavyrain")
    let code522 = Condition(code: 522, cat: "Rain", desc: "heavy intensity shower rain", title: "cloud.heavyrain")
    let code531 = Condition(code: 531, cat: "Rain", desc: "ragged shower rain", title: "cloud.heavyrain")
    let code600 = Condition(code: 600, cat: "Snow", desc: "light snow", title: "cloud.snow")
    let code601 = Condition(code: 601, cat: "Snow", desc: "Snow", title: "cloud.snow")
    let code602 = Condition(code: 602, cat: "Snow", desc: "Heavy snow", title: "snow")
    let code611 = Condition(code: 611, cat: "Snow", desc: "Sleet", title: "cloud.sleet")
    let code612 = Condition(code: 612, cat: "Snow", desc: "Light shower sleet", title: "cloud.sleet")
    let code613 = Condition(code: 613, cat: "Snow", desc: "Shower sleet", title: "cloud.sleet")
    let code615 = Condition(code: 615, cat: "Snow", desc: "Light rain and snow", title: "cloud.sleet")
    let code616 = Condition(code: 616, cat: "Snow", desc: "Rain and snow", title: "cloud.sleet")
    let code620 = Condition(code: 620, cat: "Snow", desc: "Light shower snow", title: "cloud.snow")
    let code621 = Condition(code: 621, cat: "Snow", desc: "Shower snow", title: "cloud.snow")
    let code622 = Condition(code: 622, cat: "Snow", desc: "Heavy shower snow", title: "wind.snow")
    let code701 = Condition(code: 701, cat: "Mist", desc: "mist", title: "smoke")
    let code711 = Condition(code: 711, cat: "Smoke", desc: "Smoke", title: "smoke")
    let code721 = Condition(code: 721, cat: "Haze", desc: "Haze", title: "sun.haze")
    let code731 = Condition(code: 731, cat: "Dust", desc: "sand/ dust whirls", title: "")
    let code741 = Condition(code: 741, cat: "Fog", desc: "fog", title: "cloud.fog")
    let code751 = Condition(code: 751, cat: "Sand", desc: "sand", title: "dust")
    let code761 = Condition(code: 761, cat: "Dust", desc: "dust", title: "dust")
    let code762 = Condition(code: 762, cat: "Ash", desc: "volcanic ash", title: "smoke")
    let code771 = Condition(code: 771, cat: "Squall", desc: "squalls", title: "tropicalstorm")
    let code781 = Condition(code: 781, cat: "Tornado", desc: "tornado", title: "tornado")
    let code800 = Condition(code: 800, cat: "Clear", desc: "clear sky", title: "sun.max")
    let code801 = Condition(code: 801, cat: "Clouds", desc: "few clouds: 11-25%", title: "cloud.sun")
    let code802 = Condition(code: 802, cat: "Clouds", desc: "scattered clouds: 25-50%", title: "cloud.sun")
    let code803 = Condition(code: 803, cat: "Clouds", desc: "broken clouds: 51-84%", title: "cloud.sun")
    let code804 = Condition(code: 804, cat: "Clouds", desc: "overcast clouds: 85-100%", title: "cloud")

}
