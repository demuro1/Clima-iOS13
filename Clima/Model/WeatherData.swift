//
//  WeatherData.swift
//  Clima
//
//  Created by demuro1 on 2/10/21.
//  Copyright © 2021 App Brewery. All rights reserved.
//

import Foundation

struct WeatherData: Codable {
    let name: String
    let main: Main
    let weather: [Weather]

}

struct Main: Codable {
    let temp: Double
//    let pressure: Double
    
}

struct Weather: Codable {
    let description: String
    let id: Int
}

struct Condition{
    let code: Int
    let category: String
    let description: String
    let iosTitle: String
    
    
    init(code:Int,cat:String,desc:String, title:String){
        self.code = code
        self.category = cat
        self.description = desc
        self.iosTitle = title
    }
}

