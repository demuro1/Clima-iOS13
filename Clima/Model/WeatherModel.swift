//
//  WeatherModel.swift
//  Clima
//
//  Created by demuro1 on 2/24/21.
//  Copyright © 2021 App Brewery. All rights reserved.
//

import Foundation

class WeatherModel{
    let condition: Int
    let cityName: String
    let temperature: Double
    
    var temperatureString: String { return String(format: "%.1f",temperature) }
    var conditionName: String { return getConditonName(weatherId: condition).iosTitle }
    
    init(condition:Int, cityName: String, temperature: Double) {
        self.condition = condition
        self.cityName = cityName
        self.temperature = temperature
    }
    
    let weatherCondition = WeatherCondition()
    
    func getConditonName(weatherId: Int) -> Condition {
        switch weatherId {
        case 200:
            return weatherCondition.code200
        case 201:
            return weatherCondition.code201
        case 202:
            return weatherCondition.code202
        case 210:
            return weatherCondition.code210
        case 211:
            return weatherCondition.code211
        case 212:
            return weatherCondition.code212
        case 221:
            return weatherCondition.code221
        case 230:
            return weatherCondition.code230
        case 231:
            return weatherCondition.code231
        case 232:
            return weatherCondition.code232
        case 300:
            return weatherCondition.code300
        case 301:
            return weatherCondition.code301
        case 302:
            return weatherCondition.code302
        case 310:
            return weatherCondition.code310
        case 311:
            return weatherCondition.code311
        case 312:
            return weatherCondition.code312
        case 313:
            return weatherCondition.code313
        case 314:
            return weatherCondition.code314
        case 321:
            return weatherCondition.code321
        case 500:
            return weatherCondition.code500
        case 501:
            return weatherCondition.code501
        case 502:
            return weatherCondition.code502
        case 503:
            return weatherCondition.code503
        case 504:
            return weatherCondition.code504
        case 511:
            return weatherCondition.code511
        case 520:
            return weatherCondition.code520
        case 521:
            return weatherCondition.code521
        case 522:
            return weatherCondition.code522
        case 531:
            return weatherCondition.code531
        case 600:
            return weatherCondition.code600
        case 601:
            return weatherCondition.code601
        case 602:
            return weatherCondition.code602
        case 611:
            return weatherCondition.code611
        case 612:
            return weatherCondition.code612
        case 613:
            return weatherCondition.code613
        case 615:
            return weatherCondition.code615
        case 616:
            return weatherCondition.code616
        case 620:
            return weatherCondition.code620
        case 621:
            return weatherCondition.code621
        case 622:
            return weatherCondition.code622
        case 701:
            return weatherCondition.code701
        case 711:
            return weatherCondition.code711
        case 721:
            return weatherCondition.code721
        case 731:
            return weatherCondition.code731
        case 741:
            return weatherCondition.code741
        case 751:
            return weatherCondition.code751
        case 761:
            return weatherCondition.code761
        case 762:
            return weatherCondition.code762
        case 771:
            return weatherCondition.code771
        case 781:
            return weatherCondition.code781
        case 800:
            return weatherCondition.code800
        case 801:
            return weatherCondition.code801
        case 802:
            return weatherCondition.code802
        case 803:
            return weatherCondition.code803
        case 804:
            return weatherCondition.code804
        default:
            return Condition(code: 0, cat: "none", desc: "default", title: "cloud")
        }
    }
}
